﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {
	public float startPositionY;
	public float velocity;
	public float translateDrag;
	public Transform baseTransform;
	public float moveRange;
	float newXPosition;
	float newYPosition;
	float currentMoveUpTime = 0;
	float tempNewY=0;
	bool moveUp;
	float moveUpDistance;
	// Use this for initialization
	void Start () {
		newYPosition = startPositionY;
	}
	
	// Update is called once per frame
	void Update () {

		//Para mover como una grua puedo usar rotateAround

		float pongTime = Mathf.PingPong (Time.time / translateDrag, 1);
		newXPosition = Mathf.Lerp (baseTransform.position.x - (baseTransform.localScale.x / 2) * moveRange, 
			baseTransform.position.x + (baseTransform.localScale.x / 2) * moveRange,
			pongTime);

		if (moveUp) {
			currentMoveUpTime += Time.deltaTime;
			newYPosition = Mathf.Lerp (tempNewY, tempNewY + moveUpDistance, currentMoveUpTime);
			if (newYPosition == tempNewY + moveUpDistance) {
				moveUp = false;
				currentMoveUpTime = 0;
			}
		}

		//startPositionY += Time.deltaTime*velocity;
		Vector3 newPosition = new Vector3 (newXPosition, newYPosition, transform.position.z);
		transform.position = newPosition;
		
	}
	public void MoveUp(float howMuch){
		tempNewY = newYPosition;
		moveUpDistance = howMuch;
		moveUp = true;
	}

	public void StartAgain () {
		newYPosition = startPositionY;
		tempNewY = newYPosition;
		moveUp = false;
		currentMoveUpTime = 0;
	}
}
