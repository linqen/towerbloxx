﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxesBlocking : MonoBehaviour {
	Rigidbody m_rigidBody;
	// Use this for initialization
	void Start () {
		m_rigidBody = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update () {
		m_rigidBody.velocity = new Vector3 (m_rigidBody.velocity.x,
			m_rigidBody.velocity.y,
			0);
	}
}
