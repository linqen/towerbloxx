﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

	public GameObject spawnPoint;
	public GameObject[] boxPrefabs;
	public float canSpawnTime;
	List<GameObject> instansedBoxes;
	SpawnPoint spawnPointScript;
	float currentTime = 0.0f;

	// Use this for initialization
	void Start () {
		spawnPointScript = spawnPoint.GetComponent<SpawnPoint> ();
		instansedBoxes = new List<GameObject> ();
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;
		if (Input.GetKeyDown(KeyCode.Space)&&currentTime>=canSpawnTime) {
			Vector3 positionToSpawn = new Vector3 (spawnPoint.transform.position.x, spawnPoint.transform.position.y, spawnPoint.transform.position.z);
			int randomBox = Random.Range (0, boxPrefabs.Length);
			instansedBoxes.Add((GameObject)Instantiate (boxPrefabs[randomBox], positionToSpawn, spawnPoint.transform.rotation));
			instansedBoxes[instansedBoxes.Count-1].transform.SetParent (this.transform);
			spawnPointScript.MoveUp (boxPrefabs[randomBox].transform.localScale.y);
			currentTime = 0;
			Debug.Log (instansedBoxes.Count);
			if (instansedBoxes.Count > 6) {
				Debug.Log ("Mayor a 5");
				instansedBoxes [1].GetComponent<Rigidbody> ().isKinematic = true;
				Destroy (instansedBoxes [0]);
				instansedBoxes.RemoveAt (0);
			} else if (instansedBoxes.Count == 1) {
				//instansedBoxes[0].GetComponent<Rigidbody> ().isKinematic = true;
			}
		}
	}

	public void DestroyBoxes(){
		/*int childrens = transform.childCount;
		for (int i = 0; i < childrens; i++) {
			if(transform.GetChild(i).gameObject.tag.Equals("Box"))
				Destroy(transform.GetChild(i).gameObject);
		}*/
		for (int i = 0; i < instansedBoxes.Count; i++) {
			Destroy (instansedBoxes[i]);
		}
		instansedBoxes.Clear ();
	}
}
