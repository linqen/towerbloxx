﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseCondition : MonoBehaviour {
	int collisionCount=0;
	public SpawnManager spawnManager;
	float spawnPointStartYPosition;
	SpawnPoint spawnPoint;
	// Use this for initialization
	void Start () {
		spawnPoint = spawnManager.spawnPoint.GetComponent<SpawnPoint> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnCollisionEnter(){
		collisionCount++;
		print ("Collision");
		if (collisionCount > 1) {
			print ("Perdiste");
			Invoke ("RestartGame", 1f);
		}
	}

	void RestartGame(){
		spawnPoint.StartAgain ();
		spawnManager.DestroyBoxes ();
		collisionCount = 0;
	}
}
