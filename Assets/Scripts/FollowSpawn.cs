﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowSpawn : MonoBehaviour {
	public Transform baseTransform;
	public Transform spawnPoint;
	public float cameraPositionY;
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		Vector3 newPosition = new Vector3 (baseTransform.position.x, spawnPoint.position.y-cameraPositionY, transform.position.z);
		transform.position = newPosition;
	}
}
